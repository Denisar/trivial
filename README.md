## Trivial

This app is made using the Opentdb

The Open Trivia Database provides a completely free JSON API for use in 
programming projects. Use of this API does not require a API Key, just generate 
the URL below use it in your own application to retrieve trivia questions.

https://opentdb.com/api_config.php

