package com.example.denma.trivial.GameOver;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.denma.trivial.GameActivity.GameActivity;
import com.example.denma.trivial.MainMenu.MainActivity;
import com.example.denma.trivial.R;



public class GameOverActivity extends AppCompatActivity {

    private TextView txtScore;
    private Button btnMenu;
    private GameOverPresenter gameOverPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_over_activity);

        gameOverPresenter = new GameOverPresenter(this);

        Intent intent = getIntent();
        String message = intent.getStringExtra(GameActivity.EXTRA_MESSAGE);
        String score = message;

        txtScore = findViewById(R.id.txtScore);
        btnMenu = findViewById(R.id.btnMainMenu);
        txtScore.setText(score);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameOverPresenter.backToMainMenu();
            }
        });
    }

    public void changeActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }



}
