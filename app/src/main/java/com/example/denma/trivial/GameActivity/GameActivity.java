package com.example.denma.trivial.GameActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.denma.trivial.GameOver.GameOverActivity;
import com.example.denma.trivial.MainMenu.MainActivity;
import com.example.denma.trivial.Objects.Questions;
import com.example.denma.trivial.R;

import java.util.Random;


public class GameActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "GameActivity";
    public static final String TAG = "GameActivity";

    private GamePresenter gamePresenter;
    private Button[] buttonAnswers = new Button[4];
    private TextView textQuestion;
    private TextView textQuestionNumber;
    private TextView textDifficulty;
    private TextView textLives;

    private Button correctButton;
    private Random rand = new Random();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        int categoryID = Integer.parseInt(message);


        buttonAnswers[0] = findViewById(R.id.btnAnswer1);
        buttonAnswers[1] = findViewById(R.id.btnAnswer2);
        buttonAnswers[2] = findViewById(R.id.btnAnswer3);
        buttonAnswers[3] = findViewById(R.id.btnAnswer4);

        textQuestion = findViewById(R.id.txtQText);
        textQuestionNumber = findViewById(R.id.txtQNum);
        textDifficulty = findViewById(R.id.txtDifficulty);
        textLives = findViewById(R.id.txtLives);

        gamePresenter = new GamePresenter(this,categoryID);

        updateLives();

        buttonAnswers[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gamePresenter.getAnswer(buttonAnswers[0], correctButton);

            }
        });
        buttonAnswers[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gamePresenter.getAnswer(buttonAnswers[1], correctButton);

            }
        });
        buttonAnswers[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gamePresenter.getAnswer(buttonAnswers[2], correctButton);

            }
        });
        buttonAnswers[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gamePresenter.getAnswer(buttonAnswers[3], correctButton);

            }
        });

    }

    public void displayQuestion(Questions questions){
        gamePresenter.setCanPick(true);
        updateLives();
        textQuestion.setText(questions.results.get(0).question);
        textDifficulty.setText(questions.results.get(0).difficulty);
        textQuestionNumber.setText(Integer.toString(gamePresenter.getScore()));

        int random = rand.nextInt(4);
        Log.d(TAG, "displayQuestion: "+random);
        buttonAnswers[random].setText(questions.results.get(0).correct_answer);
        correctButton = buttonAnswers[random];

        int incorrectAns = 0;
        for(int i = 0;i<4;i++){
            buttonAnswers[i].setBackgroundColor(Color.LTGRAY);
            if(buttonAnswers[i].getText() != questions.results.get(0).correct_answer){
                buttonAnswers[i].setText(questions.results.get(0).incorrect_answers.get(incorrectAns));
                incorrectAns++;
            }
        }
    }

   public void updateLives(){
       textLives.setText("Lives left: "+ Integer.toString(gamePresenter.getLives()));
   }

   public void changeActivity(){
        Intent intent = new Intent(this, GameOverActivity.class);
        intent.putExtra(EXTRA_MESSAGE,Integer.toString(gamePresenter.getScore()));
        startActivity(intent);
   }

}
