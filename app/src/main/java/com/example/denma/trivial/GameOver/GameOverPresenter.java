package com.example.denma.trivial.GameOver;

import android.support.v7.app.AppCompatActivity;

public class GameOverPresenter extends AppCompatActivity {

    private GameOverActivity gameOverActivity;

    public GameOverPresenter(GameOverActivity gameOverActivity) {
        this.gameOverActivity = gameOverActivity;
    }

    public void backToMainMenu(){
        gameOverActivity.changeActivity();
    }
}
