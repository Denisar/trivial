package com.example.denma.trivial.GameActivity;

import android.os.AsyncTask;
import android.util.Log;
import com.example.denma.trivial.MainMenu.MainMenuPresenter;
import com.example.denma.trivial.Objects.Token;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class QuestionAPI extends AsyncTask<String,Void,String> {
    private static final String TAG = "QuestionAPI";

    private String baseURL = "https://opentdb.com/api.php?amount=1&category=";
    private String categoryURL = "&difficulty=";
    private String difficultyURL = "&type=multiple";
    private String tokenURL = "&token=";

    //Full URL = baseURL + CateID + categoryURL + difficulty + difficultyURL

    private GamePresenter gamePresenter;
    private String difficulty;
    private int cateID;
    private HttpURLConnection connection;
    private InputStream inputStream;
    private BufferedReader reader;
    private Token token;

    public QuestionAPI(GamePresenter gamePresenter, int cateID, String difficulty, Token token) {
        this.cateID = cateID;
        this.difficulty = difficulty;
        this.gamePresenter = gamePresenter ;
        this.token = token;
    }

    @Override
    protected String doInBackground(String... strings) {
        try{
            URL url = new URL(baseURL + cateID + categoryURL + difficulty + difficultyURL+tokenURL+token.token);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            String line;
            inputStream = connection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            reader = new BufferedReader(new InputStreamReader(inputStream));


            while (null != (line = reader.readLine())){
                buffer.append(line).append("\n");
            }
            return buffer.toString();

        }catch (IOException e){
            Log.e(TAG, "doInBackground: ", e.getCause());
        }finally{
            try{
                inputStream.close();
                reader.close();
                connection.disconnect();
            }catch (IOException e){
                Log.e(TAG, "doInBackground: Failed to close InputStream or BufferedReader");
            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        gamePresenter.QuestionParser(s);
    }
}
