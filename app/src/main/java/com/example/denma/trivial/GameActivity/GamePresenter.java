package com.example.denma.trivial.GameActivity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import com.example.denma.trivial.Objects.Questions;
import com.example.denma.trivial.Objects.Token;
import com.google.gson.Gson;
import android.os.Handler;


public class GamePresenter extends AppCompatActivity {
    private static final String TAG = "GamePresenter";


    private final int categoryID;
    private Token token;

    private GameActivity gameActivity;
    private QuestionAPI questionAPI;

    private String[] difficulty = {"easy","medium","hard"};//Make a method that changes the difficulty
    private String currentDifficulty = difficulty[0];
    private  int score = 0;
    private int lives = 3;
    private boolean canPick = false;

    public GamePresenter(GameActivity gameActivity, int id) {
        this.categoryID = id;
        this.gameActivity = gameActivity;
        TokenCall tokenCall = new TokenCall(this);
        tokenCall.execute();
    }

    public void getQuestion(){
        if(score >= 3 && score <= 6){
            currentDifficulty = difficulty[1];
        }else if(score > 6){
            currentDifficulty = difficulty[2];
        }else{
            currentDifficulty = difficulty[0];
        }
        Log.d(TAG, "getQuestion: "+ token.token);
        questionAPI = new QuestionAPI(this,categoryID,currentDifficulty,token);
        questionAPI.execute();
    }

    public void QuestionParser(String question){
        Gson gson = new Gson();
        Questions questions = gson.fromJson(question, Questions.class);
        questions.results.get(0).fixString();
        gameActivity.displayQuestion(questions);

    }

    public void TokenParser(String tokenS){
        Gson gson = new Gson();
        Log.d(TAG, "TokenParser: "+tokenS);
        token = gson.fromJson(tokenS,Token.class);
        getQuestion();
    }

    public void getAnswer(Button button,Button correctButton) {
        if(canPick) {
            canPick = false;
            if (button == correctButton) {
                score++;
                button.setBackgroundColor(Color.GREEN);
            } else {
                lives--;
                score--;
                correctButton.setBackgroundColor(Color.GREEN);
                button.setBackgroundColor(Color.RED);
            }
            if (lives > 0) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getQuestion();
                    }
                }, 1000);
            } else {
                gameActivity.changeActivity();
            }
        }
    }
    public int getScore() {
        return score;
    }

    public int getLives() {
        return lives;
    }

    public void setCanPick(boolean canPick) {
        this.canPick = canPick;
    }
}
