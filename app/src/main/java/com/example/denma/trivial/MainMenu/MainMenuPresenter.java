package com.example.denma.trivial.MainMenu;

import android.util.Log;
import com.example.denma.trivial.Objects.Categories;
import com.google.gson.Gson;

public class MainMenuPresenter {

    private static final String TAG = "MainMenuPresenter";
    private MainActivity mainActivity;

    public MainMenuPresenter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void getCategories(){
        CategoryAPI categoryAPI = new CategoryAPI(this);
        categoryAPI.execute();
    }

    public void categoriesParser(String string){
        Log.d(TAG, string);

        Gson gson = new Gson();
        Categories categories = gson.fromJson(string,Categories.class);
        Log.d(TAG, "categoriesParser: " + categories.trivia_categories.get(1).name);
        mainActivity.setSpinner(categories);
    }
}
