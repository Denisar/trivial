package com.example.denma.trivial.MainMenu;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CategoryAPI extends AsyncTask<String,Void,String> {
    private static final String TAG = "CategoryAPI";

    private static final String catUrl = "https://opentdb.com/api_category.php";
    private MainMenuPresenter mainMenuPresenter;

    public CategoryAPI(MainMenuPresenter mainMenuPresenter) {
        this.mainMenuPresenter = mainMenuPresenter;
    }
    private HttpURLConnection connection;
    private InputStream inputStream;
    private BufferedReader reader;

    @Override
    protected String doInBackground(String... strings) {
        try {
            URL url = new URL(catUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            String line = "";
            inputStream = connection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            reader = new BufferedReader(new InputStreamReader(inputStream));

            while (null != (line = reader.readLine())) {
                buffer.append(line).append("\n");
            }
            return buffer.toString();

        } catch (IOException e) {
            Log.e(TAG, "doInBackground: ", e.getCause());
        } finally {
            connection.disconnect();
            try {
                inputStream.close();
                reader.close();
            } catch (IOException e) {
                Log.e(TAG, "doInBackground: Failed to close InputStream or BufferedReader");
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        mainMenuPresenter.categoriesParser(s);
    }
}
