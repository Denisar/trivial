package com.example.denma.trivial.GameActivity;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TokenCall extends AsyncTask<String,Void,String> {
    public static final String TAG = "TokenCall";

    GamePresenter gamePresenter;
    HttpURLConnection connection;
    BufferedReader reader;
    InputStream inputStream;

    public TokenCall(GamePresenter gamePresenter){
        this.gamePresenter = gamePresenter;
    }

    @Override
    protected String doInBackground(String... strings) {
        try{
            URL url = new URL("https://opentdb.com/api_token.php?command=request");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            String line;
            inputStream = connection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            reader = new BufferedReader(new InputStreamReader(inputStream));


            while (null != (line = reader.readLine())){
                buffer.append(line).append("\n");
            }
            return buffer.toString();

        }catch (IOException e){
            Log.e(TAG, "doInBackground: ", e.getCause());
        }finally{
            connection.disconnect();
            try{
                inputStream.close();
                reader.close();
            }catch (IOException e){
                Log.e(TAG, "doInBackground: Failed to close InputStream or BufferedReader");
            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        gamePresenter.TokenParser(s);
    }
}
