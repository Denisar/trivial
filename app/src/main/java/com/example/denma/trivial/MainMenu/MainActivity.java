package com.example.denma.trivial.MainMenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import com.example.denma.trivial.GameActivity.GameActivity;
import com.example.denma.trivial.Objects.Categories;
import com.example.denma.trivial.Objects.Category;
import com.example.denma.trivial.R;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "MainActivity";
    private static final String TAG = "MainActivity";

    private Spinner spinner;
    private Button button;
    MainMenuPresenter mainMenuPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = findViewById(R.id.spinner);
        button = findViewById(R.id.btnPlay);

        mainMenuPresenter = new MainMenuPresenter(this);
        mainMenuPresenter.getCategories();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeActivity(v);
            }
        });
    }

    public void setSpinner(Categories categories){
        if(categories == null){
            Log.e(TAG, "setSpinner: Failed to send categories to spinner");
        }
        ArrayAdapter<Category> spinnerAA =  new ArrayAdapter<Category>(this,R.layout.spinner_item,categories.trivia_categories);
        spinnerAA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAA);
    }

    private void changeActivity(View view){
        Intent intent = new Intent(this, GameActivity.class);
        String message = Integer.toString(getSpinnerData());
        intent.putExtra(EXTRA_MESSAGE,message);
        startActivity(intent);

    }

    public int getSpinnerData(){
        Category item = (Category)spinner.getSelectedItem();
        return item.id;
    }
}
