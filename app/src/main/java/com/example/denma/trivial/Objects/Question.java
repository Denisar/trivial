package com.example.denma.trivial.Objects;

import java.util.List;

public class Question {

    public String category;
    public String type;
    public String difficulty;
    public String question;
    public String correct_answer;
    public List<String> incorrect_answers;

    public String toString(){
        return question;
    }
    public void fixString(){//fixing more common question Strings
        replace("&quot;","'");
        replace("&#039;","'");
        replace("&amp;","&");
        replace("&eacute;","é");
    }

    public void replace(String target,String replacement){
        category = category.replace(target, replacement);
        type = type.replace(target, replacement);
        difficulty = difficulty.replace(target, replacement);
        question = question.replace(target, replacement);
        correct_answer = correct_answer.replace(target, replacement);
        incorrect_answers.set(0, incorrect_answers.get(0).replace(target, replacement));
        incorrect_answers.set(1, incorrect_answers.get(1).replace(target, replacement));
        incorrect_answers.set(2,incorrect_answers.get(2).replace(target, replacement));
    }
}
